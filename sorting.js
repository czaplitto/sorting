const products = [
    { name: "banana", quantity: 5, price: 10, dateOfExpire: "2020-08-08" },
    { name: "orange", quantity: 23, price: 20, dateOfExpire: "2021-07-29" },
    { name: "apple", quantity: 3, price: 8, dateOfExpire: "2021-08-29" },
    { name: "lemon", quantity: 40, price: 7, dateOfExpire: "2020-09-30" },
    { name: "peach", quantity: 200, price: 23, dateOfExpire: "2022-01-21" }
];

const textRunOut = 'We are running out of these products: ';
const textManyOfThese = 'We have many of these products: ';
const textFindPro = 'Here is the information about the products which you are looking for: ';
const textSortByQuant = "The products sort by quantity: ";
const textSortByName = "The products sort by name: ";
const textExpired = "These products are out-of-date: ";
let sortQuantity = JSON.parse(JSON.stringify(products));
let sortByName = JSON.parse(JSON.stringify(products));

const findProduct = (...productName) => {
    let arr = [];
    const nameOfAllProducts = products.map(their => their.name);
    productName.forEach((wrong) => {
        if (!nameOfAllProducts.includes(wrong)) {
            console.log(`"${wrong}" is not among products`);
        }
    });
    console.log(textFindPro);
    productName.map(one => {
        let check = products.find(elem => elem.name === one);
        !!check ? arr.push(check) : arr;
    });
    return arr;
    /*
    todo The following solution is also correct, although
    the products is not in the order that we called them.
     return products.filter(one => productName.includes(one.name))
    */
}
console.log(findProduct("apple", "hello", "banana", "lemon", "what"));
console.log(findProduct("apple", "banana", "lemon"));

function notMany() {
    let little = products.filter(elem => (elem["quantity"] < 10)).map(elem => elem.name);
    return textRunOut + `${little.join(", ")}.`;
}
console.log("\n", notMany());

function many() {
    let many = products.filter(elem => (elem["quantity"] > 20)).map(elem => elem.name);
    return textManyOfThese + `${many.join(", ")}.`;
}
console.log("\n", many());

sortQuantity.sort((a, b) => {
    if (a.quantity > b.quantity) return 1;
    else if (b.quantity > a.quantity) return -1;
    else return 0;
})
console.log("\n", textSortByQuant, "\n", sortQuantity);

sortByName.sort((a, b) => {
    if (a.name > b.name) return 1;
    else if (b.name > a.name) return -1;
    else return 0;
})
console.log("\n", textSortByName, "\n", sortByName);

function expired() {
    const giveDate = products.map(their => their.dateOfExpire);
    let create = new Date();
    let month = create.getMonth();
    let day = create.getDay();
    let year = create.getFullYear();
    let currentDate = new Date(year, month, day);
    let indexOfExpired = [];
    giveDate.map((productDate, index) => new Date(productDate) < currentDate === true ? indexOfExpired.push(index) : indexOfExpired);
    return textExpired + `${indexOfExpired.map(x => products[x].name).join(", ")}.`;
}
console.log("\n", expired());